import React from "react";
import Buttons from "./Buttons.js";

const Numpad = (props) => {
  return (
    <div>
      <Buttons getVar={props.onClick} />
    </div>
  );
};

export default Numpad;
