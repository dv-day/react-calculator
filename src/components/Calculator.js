import React from "react";
import Display from "./Display.js";
import Numpad from "./Numpad.js";

class Calculator extends React.Component {
  state = {
    value: '',
    lastVal: this.state.value.substring(-1)
  };

  showOnDisp = num => {
    if (num === "C") {
      this.setState({
        value: ''
      });
    } else if (num === "cal") {
      this.setState({
        value: eval(this.state.value)
      });
    } else if(num === "del"){
        this.setState({
            value: this.state.value.substring(0, this.state.value.length - 1)
        })
    }else{
        this.state.lastVal === '+' || this.state.lastVal === '-' || this.state.lastVal === '*' || this.state.lastVal === '/' ?
        (alert('wrong input'))
        :
        this.setState({
            value: this.state.value + num
        })
    }
  }

 

  render() {
    return (
      <div>
        <h1 style={{ textAlign: "center" }}>Calculator</h1>
        <Display value={this.state.value} />
        <Numpad onClick={this.showOnDisp} />
      </div>
    );
  }
}

export default Calculator;
