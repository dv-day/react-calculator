import React from "react";
import "./App.css";
import Calculator from "./components/Calculator.js";

const App = () => {
  return <Calculator />;
};

export default App;
