import React from "react";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Button } from "react-bootstrap";

const Buttons = props => {
  return (
    <div>
      <Row style={rowStyle}>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(1)} block>
            1
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(2)} block>
            2
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(3)} block>
            3
          </Button>
        </Col>
        <Col md="3">
          <Button variant="info" onClick={() => props.getVar("+")} block>
            +
          </Button>
        </Col>
      </Row>
      <Row style={rowStyle}>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(4)} block>
            4
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(5)} block>
            5
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(6)} block>
            6
          </Button>
        </Col>
        <Col md="3">
          <Button variant="info" onClick={() => props.getVar("-")} block>
            -
          </Button>
        </Col>
      </Row>
      <Row style={rowStyle}>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(7)} block>
            7
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(8)} block>
            8
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(9)} block>
            9
          </Button>
        </Col>
        <Col md="3">
          <Button variant="info" onClick={() => props.getVar("*")} block>
            *
          </Button>
        </Col>
      </Row>
      <Row style={rowStyle}>
        <Col md="3">
          <Button variant="danger" onClick={() => props.getVar("C")} block>
            Clear
          </Button>
        </Col>
        <Col md="3">
          <Button variant="warning" onClick={() => props.getVar(0)} block>
            0
          </Button>
        </Col>
        <Col md="3">
          <Button variant="danger" onClick={() => props.getVar("del")} block>
            Del
          </Button>
        </Col>
        <Col md="3">
          <Button variant="info" onClick={() => props.getVar("/")} block>
            /
          </Button>
        </Col>
      </Row>
      <Row style={rowStyle}>
        <Col md="12">
          <Button size="large" variant="success" onClick={() => props.getVar("cal")} block>
            =
          </Button>
        </Col>
      </Row>
    </div>
  );
};

const rowStyle = {
  margin: "20px",
};

export default Buttons;
